{-# LANGUAGE NoImplicitPrelude, GADTs , DataKinds, TypeFamilies, TypeOperators, RankNTypes, DeriveFunctor #-}

module Singletons where

import Prelude hiding (drop, take, head, tail, index, zipWith, replicate, map, (++))

data Vec a n where
  VNil :: Vec a Zero
  VCons :: a -> Vec a n -> Vec a (Succ n)

-- promoted to type level by data kinds
data Nat = Zero | Succ Nat

data SNat a where
  SZero :: SNat Zero
  SSucc :: SNat a -> SNat (Succ a)

type family (a :: Nat) :< (b :: Nat) :: Bool
type instance m :< Zero = False
type instance Zero :< Succ n = True
type instance (Succ m) :< (Succ n) = m :< n

type family (Add (a :: Nat) (b :: Nat)) :: Nat
type instance (Add Zero n) = n
type instance (Add (Succ m) n) = Succ (Add m n)

type family (Sub (a :: Nat) (b :: Nat)) :: Nat
type instance (Sub n Zero) = n
type instance (Sub Zero (Succ n)) = Zero
type instance (Sub (Succ n) (Succ m)) = Sub n m

type family (Min (a :: Nat) (b :: Nat)) :: Nat
type instance (Min Zero Zero) = Zero
type instance (Min (Succ n) Zero) = Zero
type instance (Min Zero (Succ m)) = Zero
type instance (Min (Succ n) (Succ m)) = Succ (Min n m)

map :: (a -> b) -> Vec a n -> Vec b n
map f VNil = VNil
map f (VCons x xs) = VCons (f x) (map f xs)

index :: ((a :< b) ~ True) => SNat a -> Vec s b -> s
index SZero (VCons x _) = x
index (SSucc n) (VCons _  xs) = index n xs

replicate :: s -> SNat a -> Vec s a
replicate _ SZero = VNil
replicate s (SSucc n) = VCons s (replicate s n)

-- Both vectors must be of equal length
zipWith :: (a -> b -> c) -> Vec a n -> Vec b n -> Vec c n
zipWith _ VNil _ = VNil
zipWith f (VCons x xs) (VCons y ys) = VCons (f x y) (zipWith f xs ys)

(++) :: Vec v m -> Vec v n -> Vec v (Add m n)
VNil ++ xs = xs
(VCons x xs) ++ v = VCons x (xs ++ v)

-- The semantics should match that of take for normal lists.
take :: SNat n -> Vec v m -> Vec v (Min n m)
take SZero VNil = VNil
take SZero (VCons _ _) = VNil
take (SSucc _) VNil = VNil
take (SSucc n) (VCons x xs) = VCons x (take n xs)

-- The semantics should match that of drop for normal lists.
drop :: SNat n -> Vec v m -> Vec v (Sub m n)
drop SZero VNil = VNil
drop SZero v@(VCons x xs) = v
drop (SSucc _) VNil = VNil
drop (SSucc n) (VCons _ xs) = drop n xs

head :: ((Zero :< n) ~ True) => Vec v n -> v
head (VCons x _) = x

tail :: Vec v (Succ n) -> Vec v n
tail (VCons _ xs) = xs
