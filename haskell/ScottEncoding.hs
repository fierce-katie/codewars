{-# LANGUAGE ScopedTypeVariables, Rank2Types #-}
module ScottEncoding where

import Prelude hiding (null, length, map, foldl, foldr, take, fst, snd, curry, uncurry, concat, zip, (++))

newtype SMaybe a = SMaybe { runMaybe :: forall b. b -> (a -> b) -> b }
newtype SList a = SList { runList :: forall b. b -> (a -> SList a -> b) -> b }
newtype SEither a b = SEither { runEither :: forall c. (a -> c) -> (b -> c) -> c }
newtype SPair a b = SPair { runPair :: forall c. (a -> b -> c) -> c }

emptyList :: SList a
emptyList = SList const

toPair :: SPair a b -> (a,b)
toPair (SPair mkPair) = mkPair (\a b -> (a, b))

fromPair :: (a,b) -> SPair a b
fromPair (a, b) = SPair (\mkPair -> mkPair a b)

fst :: SPair a b -> a
fst (SPair mkPair) = mkPair (\a _ -> a)

snd :: SPair a b -> b
snd (SPair mkPair) = mkPair (\_ b -> b)

swap :: SPair a b -> SPair b a
swap sPair = fromPair (snd sPair, fst sPair)

curry :: (SPair a b -> c) -> (a -> b -> c)
curry f a b = f (fromPair (a, b))

uncurry :: (a -> b -> c) -> (SPair a b -> c)
uncurry f (SPair mkPair) = mkPair f

toMaybe :: SMaybe a -> Maybe a
toMaybe (SMaybe mkMaybe) = mkMaybe Nothing (\a -> Just a)

fromMaybe :: Maybe a -> SMaybe a
fromMaybe (Just a) = SMaybe (\_ f -> f a)
fromMaybe _ = SMaybe (\b _ -> b)

isJust :: SMaybe a -> Bool
isJust (SMaybe mkMaybe) = mkMaybe False (const True)

isNothing :: SMaybe a -> Bool
isNothing (SMaybe mkMaybe) = mkMaybe True (const False)

catMaybes :: SList (SMaybe a) -> SList a
catMaybes = foldr f emptyList
  where
    f = f' . toMaybe
    f' (Just x) acc = x `cons` acc
    f' _ acc = acc

toEither :: SEither a b -> Either a b
toEither (SEither mkEither) = mkEither Left Right

fromEither :: Either a b -> SEither a b
fromEither (Left a) = SEither (\l _ -> l a)
fromEither (Right b) = SEither (\_ r -> r b)

isLeft :: SEither a b -> Bool
isLeft (SEither mkEither) = mkEither (const True) (const False)

isRight :: SEither a b -> Bool
isRight (SEither mkEither) = mkEither (const False) (const True)

partition :: SList (SEither a b) -> SPair (SList a) (SList b)
partition = fromPair . foldr f (emptyList, emptyList)
  where
    f = f' . toEither
    f' (Left l) (accl, accr) = (l `cons` accl, accr)
    f' (Right r) (accl, accr) = (accl, r `cons` accr)

toList :: SList a -> [a]
toList (SList mkList) = mkList [] (\x xs -> x : toList xs)

fromList :: [a] -> SList a
fromList [] = emptyList
fromList (x : xs) = SList (\_ c -> c x (fromList xs))

cons :: a -> SList a -> SList a
cons x xs = SList (\_ c -> c x xs)

concat :: SList a -> SList a -> SList a
concat (SList mkList) l2 = mkList l2 (\x xs -> x `cons` concat xs l2)

null :: SList a -> Bool
null (SList mkList) = mkList True (\_ _ -> False)

length :: SList a -> Int
length (SList mkList) = mkList 0 (\_ xs -> 1 + length xs)

map :: (a -> b) -> SList a -> SList b
map f (SList mkList) = mkList emptyList (\x xs -> f x `cons` map f xs)

zip :: SList a -> SList b -> SList (SPair a b)
zip l1 l2 = fromList $ zip' (toList l1) (toList l2)
  where
    zip' [] _ = []
    zip' _ [] = []
    zip' (x : xs) (y : ys) = fromPair (x, y) : zip' xs ys

foldl :: (b -> a -> b) -> b -> SList a -> b
foldl f acc (SList mkList) = mkList acc (\x xs -> foldl f (f acc x) xs)

foldr :: (a -> b -> b) -> b -> SList a -> b
foldr f acc (SList mkList) = mkList acc (\x xs -> f x (foldr f acc xs))

take :: Int -> SList a -> SList a
take 0 _ = SList (\n _ -> n)
take n (SList mkList) = mkList emptyList (\x xs -> x `cons` take (n - 1) xs)
