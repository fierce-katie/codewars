module ApplicativeParser where

import Data.Bifunctor
import Data.Char
import Prelude hiding (fmap)
import Text.Read (readMaybe)

-- | An ambiguous parser.
newtype Parser a = P { unP :: String -> [(String, a)] }

-- | Change the result of a parser.
pmap :: (a -> b) -> Parser a -> Parser b
pmap f p = P (\b -> map (second f) (unP p $ b))

-- | Operator version of 'pmap'.
(<#>) :: (a -> b) -> Parser a -> Parser b
(<#>) = pmap

-- | Parse a value and replace it.
(<#) :: a -> Parser b -> Parser a
(<#) = pmap . const

infixl 4 <#>
infixl 4 <#

-- | Parse a character only when a predicate matches.
predP :: (Char -> Bool) -> Parser Char
predP p = P f
  where
    f "" = []
    f (c : cs) | p c = [(cs, c)]
               | otherwise = []

-- | Succeed only when parsing the given character.
charP :: Char -> Parser Char
charP = predP . (==)

-- | Inject a value into an identity parser.
inject :: a -> Parser a
inject x = P (\s -> [(s, x)])

-- | Given a parser with a function value and another parser, parse the function
-- first and then the value, return a parser which applies the function to the
-- value.
(<@>) :: Parser (a -> b) -> Parser a -> Parser b
pf <@> px = P (\s -> [ (sx, f x) | (sf, f) <- unP pf $ s, (sx, x) <- unP px $ sf])

(<@) :: Parser a -> Parser b -> Parser a
pa <@ pb = P f
  where
    f s = [ (sy, x) | (sx, x) <- unP pa $ s, (sy, _) <- unP pb $ sx ]

(@>) :: Parser a -> Parser b -> Parser b
pa @> pb = P f
  where
    f s = [ res | (sx, _) <- unP pa $ s, res <- unP pb $ sx ]

infixl 4 <@
infixl 4 @>
infixl 4 <@>

-- | Parse a whole string.
stringP :: String -> Parser String
stringP s = P f
  where
    f s' | s == s' = [("", s)]
         | otherwise = []

-- | Construct a parser that never parses anything.
emptyP :: Parser a
emptyP = P (const [])

-- | Combine two parsers: When given an input, provide the results of both parser run on the input.
(<<>>) :: Parser a -> Parser a -> Parser a
(<<>>) pa pb = P (\s -> (unP pa $ s) ++ (unP pb $ s))

infixl 3 <<>>

-- | Apply the parser zero or more times.
many :: Parser a -> Parser [a]
many p = many'
  where
    many' = inject [] <<>> some'
    some' = (:) <#> p <@> many'

-- | Apply the parser one or more times.
some :: Parser a -> Parser [a]
some p = some'
  where
    many' = inject [] <<>> some'
    some' = (:) <#> p <@> many'

-- | Apply a parser and return all ambiguous results, but only those where the input was fully consumed.
runParser :: Parser a -> String -> [a]
runParser p cs = map snd (unP p $ cs)

-- | Apply a parser and only return a result, if there was only one unambiguous result with output fully consumed.
runParserUnique :: Parser a -> String -> Maybe a
runParserUnique p cs =
  let result = unP p $ cs in
    case result of
      [(_, x)] -> Just x
      _ -> Nothing


-- | Kinds of binary operators.
data BinOp = AddBO | MulBO deriving (Eq, Show)

-- | Some kind of arithmetic expression.
data Expr = ConstE Int
          | BinOpE BinOp Expr Expr
          | NegE Expr
          | ZeroE
          deriving (Eq, Show)

opFunc :: BinOp -> (Int -> Int -> Int)
opFunc AddBO = (+)
opFunc _ = (*)

evalExpr :: Expr -> Int
evalExpr (ConstE n) = n
evalExpr (BinOpE op e1 e2) = (opFunc op) (evalExpr e1) (evalExpr e2)
evalExpr (NegE e) = -(evalExpr e)
evalExpr _ = 0

exprParser :: Parser Expr
exprParser = constParser <<>> binOpExprParser <<>> negParser <<>> zeroParser
constParser = ConstE <#> intParser
intParser = P $ \s -> let res = unP (some digitParser) s in
    case res of
        [] -> []
        xs -> [second read (last xs)]
digitParser = foldr (<<>>) emptyP (map charP ['0'..'9'])
binOpExprParser = binOp <#> (charP '(' @> exprParser)
                        <@> (charP ' ' @> binOpParser <@ charP ' ')
                        <@> (exprParser <@ charP ')')
binOp e1 op e2 = BinOpE op e1 e2
binOpParser = plusParser <<>> multParser
plusParser = AddBO <# charP '+'
multParser = MulBO <# charP '*'
negParser = charP '-' @> (NegE <#> exprParser)
zeroParser = ZeroE <# charP 'z'

-- | Parse arithmetic expressions, with the following grammar:
--
--     expr         ::= const | binOpExpr | neg | zero
--     const        ::= int
--     binOpExpr    ::= '(' expr ' ' binOp ' ' expr ')'
--     binOp        ::= '+' | '*'
--     neg          ::= '-' expr
--     zero         ::= 'z'
--
parseExpr :: String -> Maybe Expr
parseExpr s = case unP exprParser s of
  [("", e)] -> Just e
  _ -> Nothing
