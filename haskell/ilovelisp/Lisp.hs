module LispLovesMe where

import Control.Applicative

import Data.Bifunctor (second)
import Data.Char
import Data.List (isPrefixOf)

data AST = I32 Int
         | Sym String
         | Nul
         | Err
         | Lst [AST]
         | Boo Bool
         | Nod AST [AST]
         deriving (Eq, Show)

-- * Parser

data Parser a = Parser { runParser :: String -> [(String, a)] }

instance Functor Parser where
  fmap f (Parser p) = Parser (\b -> map (second f) (p b))

instance Applicative Parser where
  pure x = Parser (\s -> [(s, x)])
  pf <*> px = Parser (\s -> [ (sx, f x) | (sf, f) <- runParser pf $ s,
                                          (sx, x) <- runParser px $ sf])

instance Alternative Parser where
  empty = Parser (const [])
  px <|> py = Parser (\s -> runParser px s ++ runParser py s)

skip :: (Char -> Bool) -> Parser ()
skip p = Parser (\s -> [(dropWhile p s, ())])

skipString :: String -> Parser ()
skipString s = () <$ prefixP s

predP :: (Char -> Bool) -> Parser Char
predP p = Parser f
  where
    f "" = []
    f (c : cs) | p c = [(cs, c)]
               | otherwise = []

charP :: Char -> Parser Char
charP = predP . (==)

stringP :: String -> Parser String
stringP s = Parser f
  where
    f s' | s == s' = [("", s)]
         | otherwise = []

prefixP :: String -> Parser String
prefixP s = Parser f
  where
    f input = if s `isPrefixOf` input then [(drop (length s) input, s)] else []

parseString :: Parser a -> String -> Maybe a
parseString (Parser p) s =
  case p s of
    [("", res)] -> Just res
    _ -> Nothing

whitespaces :: String
whitespaces = " ,\n\t\r"

isWhitespace :: Char -> Bool
isWhitespace = flip elem whitespaces

parserExpr :: Parser AST
parserExpr = parserSymbol
        <|> parserNumber
        <|> parserBoolean
        <|> parserNull
        <|> parserNode

parserSymbol :: Parser AST
parserSymbol = Parser f
  where
    f = mkSym . runParser p
    p = (:) <$> (predP isSymStart) <*> (many (predP isSymChar))
    isSymStart c = not (isDigit c) && isSymChar c
    isSymChar c = c `notElem` (whitespaces ++ "()")
    mkSym [] = []
    mkSym (x : _) = mkSym' x
    mkSym' (s', s) | s `elem` ["true", "false", "null"] = []
                   | otherwise = [(s', Sym s)]

parserNumber :: Parser AST
parserNumber = I32 <$> Parser f
  where
    f s = let res = runParser (some (predP isDigit)) s in
      case res of
        [] -> []
        (x : _) -> [second read x]

parserBoolean :: Parser AST
parserBoolean = mkBool <$> (prefixP "true" <|> prefixP "false")
  where
    mkBool "true" = Boo True
    mkBool "false" = Boo False
    mkBool _ = error "parserBoolean"

parserNull :: Parser AST
parserNull = Nul <$ (skipString "null"
                    <|> (charP '(' *> skip isWhitespace <* charP ')'))

parserNode :: Parser AST
parserNode = mkNode
         <$> (charP '(' *> skip isWhitespace
          *> (some (parserExpr <* skip isWhitespace))
          <* charP ')')
  where
    mkNode (x : xs) = Nod x xs
    mkNode _ = error "parserNode"

parseAST :: String -> Maybe AST
parseAST = parseString (skip isWhitespace *> parserExpr <* skip isWhitespace)

-- * Evaluation

preludeFunctions :: [(String, [AST] -> AST)]
preludeFunctions =
  [ ("+", plusF)
  , ("*", mulF)
  , ("-", minusF)
  , ("/", divF)
  , ("^", powerF)
  , (">", cmpF (>))
  , ("<", cmpF (<))
  , ("!", notF)
  , ("list", listF)
  , ("size", sizeF)
  , ("reverse", reverseF)
  , ("..", rangeF)
  , ("==", cmpF (==))
  , (">=", cmpF (>=))
  , ("<=", cmpF (<=))
  , ("!=", cmpF (/=))
  , ("if", ifF)
  ]

plusF :: [AST] -> AST
plusF = foldr plusF' (I32 0)
  where
    plusF' ast (I32 acc) = case evalAST ast of
      I32 n -> I32 (n + acc)
      _ -> Err
    plusF' _ _ = Err

mulF :: [AST] -> AST
mulF = foldr mulF' (I32 1)
  where
    mulF' ast (I32 acc) = case evalAST ast of
      I32 n -> I32 (n * acc)
      _ -> Err
    mulF' _ _ = Err

minusF :: [AST] -> AST
minusF [] = Err
minusF xs = foldl1 minusF' xs
  where
    minusF' ast1 ast2 = case (evalAST ast1, evalAST ast2) of
      (I32 n, I32 k) -> I32 (n - k)
      _ -> Err

divF :: [AST] -> AST
divF [] = Err
divF xs = foldl1 divF' xs
  where
    divF' ast1 ast2 = case (evalAST ast1, evalAST ast2) of
      (I32 n, I32 k) -> I32 (n `div` k)
      _ -> Err

unaryF :: (AST -> AST) -> [AST] -> AST
unaryF f [x] = f (evalAST x)
unaryF _ _ = Err

binaryF :: (AST -> AST -> AST) -> [AST] -> AST
binaryF f [x, y] = f (evalAST x) (evalAST y)
binaryF _ _ = Err

powerF :: [AST] -> AST
powerF = binaryF $ \x y -> case (x, y) of
  (I32 n, I32 k) -> I32 (n^k)
  _ -> Err

notF :: [AST] -> AST
notF = unaryF $ \x -> case x of
  Boo x -> Boo (not x)
  _ -> Err

listF :: [AST] -> AST
listF = Lst . map evalAST

sizeF :: [AST] -> AST
sizeF = unaryF $ \x -> case x of
  Lst l -> I32 (length l)
  _ -> Err

reverseF :: [AST] -> AST
reverseF = unaryF $ \x -> case x of
  Lst l -> Lst (reverse l)
  _ -> Err

rangeF :: [AST] -> AST
rangeF = binaryF $ \x y -> case (x, y) of
  (I32 n, I32 k) -> Lst (I32 <$> [n..k])
  _ -> Err

cmpF :: (Int -> Int -> Bool) -> [AST] -> AST
cmpF f = binaryF $ \x y -> case (x, y) of
  (I32 n, I32 k) -> Boo (f n k)
  _ -> Err

ifF :: [AST] -> AST
ifF [cond, t, f] = case evalAST cond of
  Boo b -> evalAST $ if b then t else f
  _ -> Err
ifF [cond, t] = ifF [cond, t, Nul]
ifF _ = Err

evalAST :: AST -> AST
evalAST (Nod (Sym s) cdr) = case lookup s preludeFunctions of
  Just f -> f cdr
  _ -> Err
evalAST (Nod _ _) = Err
evalAST x = x

lispEval :: String -> Maybe AST
lispEval = fmap evalAST . parseAST

-- * Pretty-printing

ppAST :: AST -> String
ppAST (I32 n) = show n
ppAST (Sym s) = s
ppAST Nul = "null"
ppAST Err = "error"
ppAST (Lst l) = ppList l
ppAST (Boo True) = "true"
ppAST (Boo False) = "false"
ppAST (Nod car cdr) = ppList (car : cdr)

ppList :: [AST] -> String
ppList [] = "null"
ppList l = '(' : ppList' l ++ ")"
 where
   ppList' [x] = ppAST x
   ppList' (x : xs) = ppAST x ++ " " ++ ppList' xs

lispPretty :: String -> Maybe String
lispPretty = fmap ppAST . parseAST
