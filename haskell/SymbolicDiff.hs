module SymbolicDifferentiationOfPrefixExpressions (diff) where

import Control.Applicative hiding (Const)

import Data.Char (isDigit)
import Data.Functor
import Data.Bifunctor (second)
import Data.List (isPrefixOf)

import Text.Read

data Expr
  = Const Int
  | Var
  | Plus Expr Expr
  | Minus Expr Expr
  | Mul Expr Expr
  | Div Expr Expr
  | Pow Expr Expr
  | Cos Expr
  | Sin Expr
  | Tan Expr
  | Exp Expr
  | Ln Expr
  | Error String
  | ConstF Float

instance Show Expr where
  show (Const n) = show n
  show (ConstF n) = show n
  show Var = "x"
  show (Plus e1 e2)  = "(+ "  ++ show e1 ++ " " ++ show e2 ++ ")"
  show (Minus e1 e2) = "(- "  ++ show e1 ++ " " ++ show e2 ++ ")"
  show (Mul e1 e2)   = "(* "  ++ show e1 ++ " " ++ show e2 ++ ")"
  show (Div e1 e2)   = "(/ "  ++ show e1 ++ " " ++ show e2 ++ ")"
  show (Pow e1 e2)   = "(^ "  ++ show e1 ++ " " ++ show e2 ++ ")"
  show (Cos e) = "(cos " ++ show e ++ ")"
  show (Sin e) = "(sin " ++ show e ++ ")"
  show (Tan e) = "(tan " ++ show e ++ ")"
  show (Exp e) = "(exp " ++ show e ++ ")"
  show (Ln e)  = "(ln " ++ show e ++ ")"
  show (Error s) = s

diffExpr :: Expr -> Expr
diffExpr (Const _) = Const 0
diffExpr Var = Const 1
diffExpr (Plus e1 e2) = Plus (diffExpr e1) (diffExpr e2)
diffExpr (Minus e1 e2) = Minus (diffExpr e1) (diffExpr e2)
diffExpr (Mul e1 e2) = Plus (Mul (diffExpr e1) e2) (Mul e1 (diffExpr e2))
diffExpr (Div e1 e2) = Div
  (Minus
    (Mul (diffExpr e1) e2)
    (Mul e1 (diffExpr e2))
  )
  (Pow e2 (Const 2))
diffExpr (Pow e1 e2) = Mul (diffExpr e1) (Mul e2 (Pow e1 (Minus e2 (Const 1))))
diffExpr (Cos e) = Mul (diffExpr e) (Mul (Const (-1)) (Sin e))
diffExpr (Sin e) = Mul (diffExpr e) (Cos e)
diffExpr (Tan e) = Mul (diffExpr e) (Plus (Const 1) (Pow (Tan e) (Const 2)))
diffExpr (Exp e) = Mul (diffExpr e) (Exp e)
diffExpr (Ln e) = Div (diffExpr e) e
diffExpr (Error e) = Error e

newtype Parser a = Parser { unParser :: String -> [(String, a)] }

instance Functor Parser where
  fmap f (Parser p) = Parser (\b -> map (second f) (p b))

instance Applicative Parser where
  pure x = Parser (\s -> [(s, x)])
  pf <*> px = Parser (\s -> [ (sx, f x) | (sf, f) <- unParser pf $ s,
                                          (sx, x) <- unParser px $ sf])

instance Alternative Parser where
  empty = Parser (const [])
  px <|> py = Parser (\s -> unParser px s ++ unParser py s)

predP :: (Char -> Bool) -> Parser Char
predP p = Parser f
  where
    f "" = []
    f (c : cs) | p c = [(cs, c)]
               | otherwise = []

charP :: Char -> Parser Char
charP = predP . (==)

prefixP :: String -> Parser String
prefixP s = Parser f
  where
    f input = if s `isPrefixOf` input
                then [(drop (length s) input, s)]
                else []
stringP :: String -> Parser String
stringP s = Parser f
  where
    f s' | s == s' = [("", s)]
         | otherwise = []

parseStr :: String -> Expr
parseStr s = case unParser exprParser s of
  [("", e)] -> e
  _         -> Error ("no parse: " ++ s)
  where
    exprParser = constParser <|> varParser <|> binOpParser <|> unOpParser
    constParser = Const <$> intParser
    intParser = Parser (f 1)
      where
        f 1 ('-':s) = f (-1) s
        f x s = let (n, rest) = span isDigit s in
                  case readMaybe n :: Maybe Int of
                    Just n' -> [(rest, x * n')]
                    _ -> []
    varParser = prefixP "x" $> Var
    binOpParser = (
      (prefixP "(+ " *> (Plus  <$> (exprParser <* charP ' ') <*> exprParser)) <|>
      (prefixP "(- " *> (Minus <$> (exprParser <* charP ' ') <*> exprParser)) <|>
      (prefixP "(* " *> (Mul   <$> (exprParser <* charP ' ') <*> exprParser)) <|>
      (prefixP "(/ " *> (Div   <$> (exprParser <* charP ' ') <*> exprParser)) <|>
      (prefixP "(^ " *> (Pow   <$> (exprParser <* charP ' ') <*> exprParser))) <*
      charP ')'
    unOpParser = (
      (prefixP "(cos " *> (Cos  <$> exprParser)) <|>
      (prefixP "(sin " *> (Sin  <$> exprParser)) <|>
      (prefixP "(tan " *> (Tan  <$> exprParser)) <|>
      (prefixP "(exp " *> (Exp  <$> exprParser)) <|>
      (prefixP "(ln "  *> (Ln   <$> exprParser))) <*
      charP ')'

simplify :: Expr -> (Bool, Expr)
simplify (Plus (Const 0) e) = (True, snd $ simplify e)
simplify (Plus e (Const 0)) = (True, snd $ simplify e)
simplify (Plus (Const n) (Const m)) = (True, Const (n + m))
simplify (Plus e1 e2) =
  let (f1, s1) = simplify e1
      (f2, s2) = simplify e2
  in
      if (f1 || f2) then simplify (Plus s1 s2) else (False, Plus s1 s2)
simplify (Minus e (Const 0)) = (True, snd $ simplify e)
simplify (Minus (Const n) (Const m)) = (True, Const (n - m))
simplify (Minus e1 e2) =
  let (f1, s1) = simplify e1
      (f2, s2) = simplify e2
  in
      if (f1 || f2) then simplify (Minus s1 s2) else (False, Minus s1 s2)
simplify (Mul (Const 0) _) = (True, Const 0)
simplify (Mul _ (Const 0)) = (True, Const 0)
simplify (Mul (Const 1) e) = (True, snd $ simplify e)
simplify (Mul e (Const 1)) = (True, snd $ simplify e)
simplify (Mul (Const n) (Const m)) = (True, Const (n * m))
simplify (Mul e1 e2) =
  let (f1, s1) = simplify e1
      (f2, s2) = simplify e2
  in
      if (f1 || f2) then simplify (Mul s1 s2) else (False, Mul s1 s2)
simplify (Div (Const 0) _) = (True, Const 0)
simplify (Div e (Const 1)) = (True, snd $ simplify e)
simplify (Div (Const n) (Const m)) = (True, ConstF (fromIntegral n / fromIntegral m))
simplify (Div e1 e2) =
  let (f1, s1) = simplify e1
      (f2, s2) = simplify e2
  in
      if (f1 || f2) then simplify (Div s1 s2) else (False, Div s1 s2)
simplify (Pow (Const 0) _) = (True, Const 0)
simplify (Pow (Const 1) _) = (True, Const 1)
simplify (Pow e (Const 0)) = (True, Const 1)
simplify (Pow e (Const 1)) = (True, snd $ simplify e)
simplify (Pow (Const n) (Const m)) = (True, Const (n^m))
simplify (Pow e1 e2) =
  let (f1, s1) = simplify e1
      (f2, s2) = simplify e2
  in
      if (f1 || f2) then simplify (Pow s1 s2) else (False, Pow s1 s2)
simplify e = (False, e)

diff :: String -> String
diff = show . snd . simplify . diffExpr . parseStr
