-- https://www.codewars.com/kata/599aed42b9712e1afe000014
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies  #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE UndecidableInstances  #-}
module PolyvariadicFunctions where

class PolySum a where
  polySum :: Int -> a

instance PolySum Int where
  polySum = id

instance (Integral n, PolySum a) => PolySum (n -> a) where
  polySum acc = \m -> polySum (fromIntegral m + acc)

-- `polyAdd` sums its arguments, all `Int`s.
polyAdd :: PolySum a => a
polyAdd = polySum 0

class PolyLst a acc | a -> acc where
  polyLst :: [acc] -> a

instance PolyLst [a] a where
  polyLst = id

instance PolyLst a acc => PolyLst (acc -> a) acc where
  polyLst xs = \x -> polyLst (xs ++ [x])

-- `polyList` turns its arguments into a list, polymorphically.
polyList :: PolyLst a acc => a
polyList = polyLst []

class PolyStr a where
  polyStr :: [String] -> a

instance PolyStr String where
  polyStr = unwords

instance PolyStr a => PolyStr (String -> a) where
  polyStr acc = \s -> polyStr (acc ++ [s])

-- `polyAdd` sums its arguments, all `Int`s.
-- `polyWords` turns its arguments into a spaced string.
polyWords :: PolyStr a => a
polyWords = polyStr []
