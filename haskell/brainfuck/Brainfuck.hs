{-# LANGUAGE RecordWildCards #-}
module Brainfuck
    ( executeString
    ) where

import Data.Char (chr, ord)
import Data.Function ((&))
import Data.List (find, elemIndices)
import Data.Word (Word8)

-- | Brainfuck instructions.
data Instruction
    = IncDataPtr
    | DecDataPtr
    | IncDataValue
    | DecDataValue
    | OutputDataValue
    | AcceptInput
    | JumpZeroForward
    | JumpNonZeroBack
    | Unknown
    deriving (Eq, Show)

-- | Program code is a list of instructions.
type Code = [Instruction]

-- | Program memory is an infinite list of bytes.
type Memory = [Word8]

-- | Action preformed for each instruction. Returns Nothing ain case of error.
type InstructionAction = Program -> Maybe Program

-- | Program state representation.
data Program = Program
    { programCode    :: Code   -- Program code left to be run.
    , programCodePtr :: Int    -- Instruction pointer.
    , programData    :: Memory -- Program memory space.
    , programDataPtr :: Int    -- Current position in memory.
    , programInput   :: String -- Program input that hasn't been processed.
    , programOutput  :: String -- Program output (REVERSED).
    } deriving Show

-- | Increment data pointer.
incDataPtr :: InstructionAction
incDataPtr p@Program{..} = Just p { programDataPtr = succ programDataPtr }

-- | Decrement data pointer.
decDataPtr :: InstructionAction
decDataPtr p@Program{..} =
    if newDataPtr < 0
        then Nothing
        else Just p { programDataPtr = newDataPtr }
    where
        newDataPtr = pred programDataPtr

-- | Increment byte at data pointer.
incDataValue :: InstructionAction
incDataValue p@Program{..} = Just p
        { programData = modifyAt programDataPtr succW programData }

-- | Decrement byte at data pointer.
decDataValue :: InstructionAction
decDataValue p@Program{..} = Just p
        { programData = modifyAt programDataPtr predW programData }

-- | Add byte at data pointer to output.
outputDataValue :: InstructionAction
outputDataValue p@Program{..} = Just p
        { programOutput = word8ToChar (programData !! programDataPtr) : programOutput }

-- | Save char from input to memory at current data pointer.
acceptInput :: InstructionAction
acceptInput p@Program{..} = case programInput of
    [] -> Nothing
    i  -> Just p
        { programInput = tail i
        , programData  = modifyAt programDataPtr (const (charToWord8 (head i))) programData
        }

-- | If the byte at the data pointer is zero, jump forward to the command after
-- the matching ']'.
jumpZeroForward :: InstructionAction
jumpZeroForward p@Program{..} =
    if (programData !! programDataPtr) == 0
        then updateCodePtr <$> jumpForward'
        else Just p
    where
        updateCodePtr ip = p { programCodePtr = ip }
        jumpForward' = jumpForward programCodePtr
                                   (drop programCodePtr programCode)
                                   0

-- | Find matching ']'.
jumpForward :: Int -> Code -> Int -> Maybe Int
jumpForward _ [] _ = Nothing
jumpForward ptr (JumpZeroForward : xs) n = jumpForward (ptr + 1) xs (n + 1)
jumpForward ptr (JumpNonZeroBack : xs) n = let n' = n - 1 in
    if n' == 0 then Just ptr else jumpForward (ptr + 1) xs n'
jumpForward ptr (_ : xs) n = jumpForward (ptr + 1) xs n

-- | If the byte at the data pointer is nonzero, jump back to the command after
-- the matching '['.
jumpNonZeroBack :: InstructionAction
jumpNonZeroBack p@Program{..} =
    if (programData !! programDataPtr) == 0
        then Just p
        else updateCodePtr <$> jumpBackward'
    where
        updateCodePtr ip = p { programCodePtr = ip }
        jumpBackward' = jumpBackward (programCodePtr - 1)
                                     (reverse (take programCodePtr programCode))
                                     1

-- | Find matching '['.
jumpBackward :: Int -> Code -> Int -> Maybe Int
jumpBackward _ [] _ = Nothing
jumpBackward ptr (JumpNonZeroBack : xs) n = jumpBackward (ptr - 1) xs (n + 1)
jumpBackward ptr (JumpZeroForward : xs) n = let n' = n - 1 in
    if n' == 0 then Just ptr else jumpBackward (ptr - 1) xs n'
jumpBackward ptr (_ : xs) n = jumpBackward (ptr - 1) xs n

-- | Execute single instruction.
execInstruction :: Instruction -> Program -> Maybe Program
execInstruction instr prog = prog & case instr of
    IncDataPtr      -> incDataPtr
    DecDataPtr      -> decDataPtr
    IncDataValue    -> incDataValue
    DecDataValue    -> decDataValue
    OutputDataValue -> outputDataValue
    AcceptInput     -> acceptInput
    JumpZeroForward -> jumpZeroForward
    JumpNonZeroBack -> jumpNonZeroBack
    Unknown         -> const Nothing

-- | Perform one execution step.
step :: Maybe Program -> Maybe Program
step Nothing = Nothing
step jp@(Just prog@Program{..}) =
    if programCodePtr >= length programCode
        then jp
        else step newProg
    where
        newProg' = execInstruction (programCode !! programCodePtr) prog
        newProg = (\p@Program{..} -> p { programCodePtr = succ programCodePtr })
                      <$> newProg'

-- | Initialise program execution state.
initProgramState :: Code -> String -> Program
initProgramState code input = Program code 0 [0, 0..] 0 input ""

-- | Convert character to instruction.
charToInstruction :: Char -> Instruction
charToInstruction '>' = IncDataPtr
charToInstruction '<' = DecDataPtr
charToInstruction '+' = IncDataValue
charToInstruction '-' = DecDataValue
charToInstruction '.' = OutputDataValue
charToInstruction ',' = AcceptInput
charToInstruction '[' = JumpZeroForward
charToInstruction ']' = JumpNonZeroBack
charToInstruction _   = Unknown

-- | Convert string to program code.
stringToCode :: String -> Code
stringToCode = map charToInstruction

-- | Convert 8-bit word to char.
word8ToChar :: Word8 -> Char
word8ToChar = chr . fromIntegral

-- | Convert char to 8-bit word.
charToWord8 :: Char -> Word8
charToWord8 = fromIntegral . ord

-- | Wrapped version of 'pred' for Word8.
predW :: Word8 -> Word8
predW x = if x == minBound then maxBound else pred x

-- | Wrapped version of 'succ' for Word8.
succW :: Word8 -> Word8
succW x = if x == maxBound then minBound else succ x

-- | Modify list element at given position.
modifyAt :: Int -> (a -> a) -> [a] -> [a]
modifyAt n f xs = xs1 ++ (x : xs2)
    where
        xs1 = take n xs
        x   = f (xs !! n)
        xs2 = drop (n + 1) xs

-- | Run the code.
executeCode :: Program -> Maybe String
executeCode p = reverse . programOutput <$> step (Just p)

-- | Interprets the Brainfuck source code from the first argument, while
-- supplying it with input from the second. May fail on insufficient input.
executeString :: String -> String -> Maybe String
executeString source input = executeCode (initProgramState (stringToCode source) input)

